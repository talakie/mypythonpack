from setuptools import setup, find_packages

setup(
	packages = find_packages(exclude = ['doc', 'example']),
	entry_points = {
		'console_scripts':
		['mypythonpack-myfunc = mypythonpack.myfunc:main']}
)
